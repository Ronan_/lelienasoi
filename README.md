Static starter
==============

Static starter project used at Drosalys company. Perfect for landing/one static page.

## Install

Replace `project_name` by the name of the projet you when to create.

```bash
git clone git@gitlab.com:drosalys/starters/static.git project_name
yarn
```

## Available commands

### Development mode

Start a dev web server b default at http://localhost:4300

```bash
yarn serve
```

### Prod build

Build the static app in the `dist` directory.

```bash
yarn build
```

## Maintainers

- The [Drosalys](https://www.drosalys-web.fr/) company
- [Benjamin Georgeault](https://gitlab.com/WedgeSama)

## LICENSE

The code used to generate this stack is release under MIT licence.

## TODO

- Auto handle multiple html files 
